<?php

use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use League\Fractal\Serializer\DataArraySerializer;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\DatabaseMigrations;

abstract class FunctionalApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions, DatabaseMigrations;

    abstract public static function getClass();

    abstract public function getTransformer();

    public function getIdentifier()
    {
        return 'id';
    }

    public function getJsonFields()
    {
        return false;
    }

    /**
     * Create fake instance of model and save it in database
     *
     * @param array $fields
     * @return Model
     */
    public function make($fields = [])
    {
        $model = $this->fake($fields);

        $model->save();

        return $model;
    }

    /**
     * Get fake instance of model
     *
     * @param array $fields
     * @return Model
     */
    public function fake($fields = [])
    {
        $fake = populator(static::getClass())->make();

        return $fake;
    }

    /**
     * Get fake data of CaseContext
     *
     * @param array $postFields
     * @return array
     */
    public function fakeData($fields = [])
    {
        $fake = $this->fake($fields);

        $fakeArray = $fake->toArray();

        if (isset($fakeArray['id'])) {
          unset($fakeArray['id']);
        }

        return array_merge($fakeArray, $fields);
    }
}
