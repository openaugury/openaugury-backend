# ORP and WebLate synchronization

Foobar is a Python library for dealing with word pluralization.

## 1. Backend

1.1 Extract texts (strings) by ```make extract_strings``` and it will create lang_backend.pot (template file for weblate)

1.2 Go to ```resources/openaugury-translations``` submodule and push created lang_backend.pot to the repository.

## 2. Frontend

2.1 Extract texts (strings) by ```make extract_strings``` and it will create lang_frontend.pot (template file for weblate)

2.2 Go to ```src/locales/openaugury-translations``` submodule and push created lang_frontend.pot 	to repository.

## 3. Merge created 2 (lang_frontend and lang_backend) files to one.
3.1. Pull backend submodule to get lang_frontend and frontend for lang_backend.

3.2. After pull we need to have lang_frontend and lang_backend in both sides. We will merge them and replace lang.pot file with new one. For that we should run ‘make merge_pot_files’ command in both folders backend and frontend.

#### 3.3. If language already created in Weblate then we have to merge also languages with lang.pot for append new words and annotations.(These commands needs to be done only one folder backend or frontend)
 
- 3.3.1. Copy lang.pot file to each language folder with ```make copy_pot_to_language_folders``` command.

- 3.3.2. Change extension from .pot to .po with ```make change_format_pot_to_po``` command.
	
- 3.3.3. Merge .po file each language folder with ```make merge_po_files``` command.
	
- 3.3.4. Remove lang.po files from language folders with ```make remove_template_po``` command.

#### 3.4. Push created lang.pot and merged language files to repository for weblate integration.
- 3.4.1. Pull changes to submodule.

#### 4. Compile .po files in frontend and get translations.json file.

- 4.1. Go to src/locales folder in frontend and run “gettext-compile --output translations.json $(find $PWD/openaugury-translations/resources/lang/i18n/ -type f -name '*.po')”

#### 5. After all changes sign in to Weblate and integrate Weblate from Manage→Repository Maintenance→Update.
