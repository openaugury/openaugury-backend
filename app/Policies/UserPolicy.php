<?php

namespace App\Policies;

use App\Models\Users\User;
use App\Models\UserTier\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    public function before(User $user, $ability)
    {
        if ($user->isAdministrator()) {
            return true;
        }
    }

    /**
     * Determine whether the user can view any subjects.
     *
     * @param  \App\Models\Users\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can view the subject.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\User  $subject
     * @return mixed
     */
    public function view(User $user, User $subject)
    {
        return $user->id == $subject->id;
    }

    /**
     * Determine whether the user can create subjects.
     *
     * @param  \App\Models\Users\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can update the subject.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\User  $subject
     * @return mixed
     */
    public function update(User $user, User $subject)
    {
        return $user->id === $subject->id;
    }

    /**
     * Determine whether the user can delete the subject.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\User  $subject
     * @return mixed
     */
    public function delete(User $user, User $subject)
    {
        return false;
    }

    /**
     * Determine whether the user can restore the subject.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\User  $subject
     * @return mixed
     */
    public function restore(User $user, User $subject)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the subject.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\User  $subject
     * @return mixed
     */
    public function forceDelete(User $user, User $subject)
    {
        return false;
    }
}
