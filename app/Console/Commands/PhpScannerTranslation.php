<?php

namespace App\Console\Commands;

use Gettext\Generator\PoGenerator;
use Gettext\Scanner\PhpScanner;
use Gettext\Translations;
use Illuminate\Console\Command;
use Illuminate\View\Compilers\BladeCompiler;

class PhpScannerTranslation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'orp:translate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(BladeCompiler $bladeCompiler)
    {
        //Create a new scanner, adding a translation for each domain we want to get:
        $phpScanner = new PhpScanner(
            Translations::create('en')
        );

        //Set a default domain, so any translations with no domain specified, will be added to that domain
        $phpScanner->setDefaultDomain('en');

        $phpScanner->setFunctions([
            '_' => 'gettext',
            '_i' => 'gettext',
        ]);

        $dataDir = resource_path() . '/views/';

        try
        {
            $dir = new \DirectoryIterator($dataDir);

            foreach ($dir as $folderName) {
                if (!$folderName->isDot() and $folderName != "welcome.blade.php" and
                    $folderName != "home.blade.php" and $folderName != ".gitignore") {
                    foreach (glob($dataDir.$folderName.'/*.blade.php') as $fileName) {                        
                        $phpString = $bladeCompiler->compileString(\File::get($fileName));
                        $phpScanner->scanString($phpString, $fileName);
                    }
                }

            }
            $phpString = $bladeCompiler->compileString(\File::get($dataDir.'home.blade.php'));
            $phpScanner->scanString($phpString, $dataDir.'home.blade.php');
        } catch (Exception $ex) {
            error_log($ex);
        }

        //Save the translations in .po files
        $generator = new PoGenerator();

        foreach ($phpScanner->getTranslations() as $domain => $translations) {
            $generator->generateFile($translations, resource_path() . "/openaugury-translations/resources/lang/i18n/lang_backend.pot");
        }
    }
}
