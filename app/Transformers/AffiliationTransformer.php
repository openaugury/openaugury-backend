<?php

namespace App\Transformers;

use App\Models\Users\Affiliation;
use League\Fractal;

/**
 * @SWG\Definition(
 *      definition="Affiliation",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="slug",
 *          description="slug",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="district_id",
 *          description="district_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class AffiliationTransformer extends Fractal\TransformerAbstract
{
    public function transform(Affiliation $affiliation)
    {
        $affiliationArray = $affiliation->toArray();

        return [
            'id' => $affiliationArray['id'],
            'slug' => $affiliationArray['slug'],
            'name' => $affiliationArray['name'],
            'district_id' => $affiliationArray['district_id'],
            'code' => $affiliationArray['code'],
            'updated_at' => $affiliationArray['updated_at'],
            'created_at' => $affiliationArray['created_at'],
        ];
    }
}
