<?php

namespace App\Transformers;

use App\Models\Features\FeatureSet;
use App\Models\Features\District;
use League\Fractal;

/**
 * @SWG\Definition(
 *      definition="FeatureSet",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="slug",
 *          description="slug",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="description",
 *          description="description",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class FeatureSetTransformer extends Fractal\TransformerAbstract
{
    public function transform(FeatureSet $featureSet)
    {
        $featureSetArray = $featureSet->toArray();

        return [
            'id' => $featureSetArray['slug'],
            'name' => $featureSetArray['name'],
            'description' => $featureSetArray['description'],
            'created_at' => $featureSetArray['created_at'],
            'updated_at' => $featureSetArray['updated_at']
        ];
    }
}
