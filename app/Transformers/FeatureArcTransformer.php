<?php

namespace App\Transformers;

use App\Models\SimulationTier\FeatureArc;
use League\Fractal;

/**
 * @SWG\Definition(
 *      definition="FeatureArc",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="feature_chunk_id",
 *          description="feature_chunk_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="simulation_id",
 *          description="Simulation UUID",
 *          type="string",
 *          format="uuid"
 *      ),
 *      @SWG\Property(
 *          property="arc",
 *          description="ordered list of effects on each feature in the matching chunk",
 *          type="array",
 *          format="array"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class FeatureArcTransformer extends Fractal\TransformerAbstract
{
    public function transform(FeatureArc $featureArc)
    {
        return [
            'id' => $featureArc->id,
            'simulation_id' => $featureArc->simulation_id,
            'feature_chunk_id' => $featureArc->feature_chunk_id,
            'created_at' => $featureArc->created_at,
            'updated_at' => $featureArc->updated_at
        ];
    }
}
