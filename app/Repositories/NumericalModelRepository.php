<?php

namespace App\Repositories;

use App\Models\AbstractTier\NumericalModel;
use InfyOm\Generator\Common\BaseRepository;

class NumericalModelRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'definition',
        'phenomenon_id',
        'owner_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return NumericalModel::class;
    }
}
