<?php

namespace App\Repositories;

use App\Models\Interaction\Challenge;
use A17\Twill\Repositories\ModuleRepository;
use A17\Twill\Repositories\Behaviors\HandleFiles;
use A17\Twill\Repositories\Behaviors\HandleMedias;

class ChallengeRepository extends ModuleRepository
{
    use HandleFiles, HandleMedias;
    
    public function __construct(Challenge $model)
    {
        $this->model = $model;
    }

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'text',
        'subtext',
        'options',
        'correct',
        'mark',
        'challenge_type',
        'challenge_level',
        'challenge_template_id',
        'simulation_id',
        'result_analytic_id'
    ];

    public function prepareFieldsBeforeSave($object, $fields) {

        if ($fields["challenge_type"] == "multiple_choice"
        and array_key_exists("option0", $fields) and array_key_exists("option1", $fields)
        and array_key_exists("option2", $fields) and array_key_exists("option3", $fields)) {
            $object->setOption0Attribute($fields["option0"]);
            $object->setOption1Attribute($fields["option1"]);
            $object->setOption2Attribute($fields["option2"]);
            $object->setOption3Attribute($fields["option3"]);
        }   
        return parent:: prepareFieldsBeforeSave($object, $fields);
    }
    
}
