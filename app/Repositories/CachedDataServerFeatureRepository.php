<?php

namespace App\Repositories;

use App\Models\CachedDataServerFeature;
use InfyOm\Generator\Common\BaseRepository;

class CachedDataServerFeatureRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'cached_data_server_feature_set_id',
        'feature_id',
        'location',
        'extent',
        'json'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CachedDataServerFeature::class;
    }
}
