<?php

namespace App\Repositories;

use App\Models\SimulationTier\Simulation;
use A17\Twill\Repositories\ModuleRepository;

class SimulationRepository extends ModuleRepository
{

    public function __construct(Simulation $model)
    {
        $this->model = $model;
    }

    /**
     * @var array
     */

    protected $fieldSearchable = [
        'case_context_id',
        'combination_id',
        'settings',
        'definition',
        'status',
        'center',
        'extent',
        'begins',
        'ends',
        'result',
        'owner_id'
    ];

    public function prepareFieldsBeforeCreate($fields) {
        if (isset($fields['status']) && is_string($fields['status']) && substr($fields['status'], 0, 7) === 'STATUS_') {
            $fields['status'] = constant(Simulation::class . '::' . $fields['status']);
        }

        return parent:: prepareFieldsBeforeCreate($fields);
    }
}
