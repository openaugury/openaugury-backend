<?php

namespace App\Repositories;

use App\Models\AbstractTier\Resource;
use InfyOm\Generator\Common\BaseRepository;

class ResourceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'code',
        'name',
        'description'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Resource::class;
    }
}
