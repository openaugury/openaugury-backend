<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class SimulationCreated
{
    use InteractsWithSockets, SerializesModels;

    /**
     * Simulation that has been created
     *
     * @val Simulation
     */
    protected $simulation;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Simulation $simulation)
    {
        $this->simulation = $simulation;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        // return new PrivateChannel('channel-name');
    }

    /**
     * Get created simulation
     *
     * @return Simulation
     */
    public function getSimulation()
    {
        return $simulation;
    }
}
