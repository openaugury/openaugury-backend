<?php

namespace App\Models\Interaction;

use Eloquent as Model;


/**
 * @SWG\Definition(
 *      definition="WorksheetAnswer",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="mark",
 *          description="mark",
 *          type="number",
 *          format="float"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class WorksheetAnswer extends Model
{


    public $table = 'worksheet_answers';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'first',
        'final',
        'mark',
        'worksheet_id',
        'challenge_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * Challenge to which this answer applies
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function challenge()
    {
        return $this->belongsTo(\App\Models\Interaction\Challenge::class, 'challenge_id', 'id');
    }

    /**
     * Student worksheet containing this answer
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function worksheet()
    {
        return $this->belongsTo(\App\Models\Interaction\Worksheet::class, 'worksheet_id', 'id');
    }
}
