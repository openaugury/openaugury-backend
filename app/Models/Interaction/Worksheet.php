<?php

namespace App\Models\Interaction;

use Eloquent as Model;

use Alsofronie\Uuid\UuidModelTrait;

/**
 * @SWG\Definition(
 *      definition="Worksheet",
 *      required={""},
 *      @SWG\Property(
 *          property="progress",
 *          description="progress",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Worksheet extends Model
{

    use UuidModelTrait;

    const PROGRESS_INACTIVE = 0; /* Still a placeholder */
    const PROGRESS_NOT_READY = 1; /* Not released by teacher */
    const PROGRESS_READY = 2; /* Ready for student to interact with */
    const PROGRESS_STARTED = 3; /* In progress */
    const PROGRESS_COMPLETED = 4; /* All questions answered */
    const PROGRESS_SUBMITTED = 5; /* Ready for attention of teacher */
    const PROGRESS_MARKED = 6; /* Student can view marks */
    const PROGRESS_REOPENED = 7; /* Returned to student */

    public $table = 'worksheets';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'progress',
        'simulation_id',
        'worker_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'progress' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * User who is filling these answers
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function worker()
    {
        return $this->belongsTo(\App\Models\Users\User::class, 'worker_id', 'id');
    }

    /**
     * Simulation to which this belongs
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function simulation()
    {
        return $this->belongsTo(\App\Models\SimulationTier\Simulation::class, 'simulation_id', 'id');
    }

    /**
     * Simulation challenges and their answers that are attached to this worksheet
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function worksheets()
    {
        return $this->belongsToMany(\App\Models\Interaction\Worksheet::class, 'worksheet_answers');
    }
}
