<?php

namespace App\Models\Revisions;

use A17\Twill\Models\Revision;

class SimulationRevision extends Revision
{
    protected $table = "simulation_revisions";
}
