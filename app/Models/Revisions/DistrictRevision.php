<?php

namespace App\Models\Revisions;

use A17\Twill\Models\Revision;

class DistrictRevision extends Revision
{
    protected $table = "district_revisions";
}
