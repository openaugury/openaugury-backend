<?php

namespace App\Http\Controllers\API;
use Log;
use Swis\JsonApi\Client\Parsers\DocumentParser;
use App\Http\Requests\API\CreateCombinationAPIRequest;
use App\Http\Requests\API\UpdateCombinationAPIRequest;
use App\Models\AbstractTier\Combination;
use App\Repositories\CombinationRepository;
use App\Repositories\NumericalModelRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Transformers\CombinationTransformer;
use App\Transformers\ResultFeatureChunkTransformer;
use App\Transformers\InformationalTransformer;
use App\Transformers\ChallengeTransformer;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Symfony\Component\HttpFoundation\Response;
use DB;
use Auth;
use Spatie\Fractal\Fractal;
use App\Jobs\CombinationRun;

/**
 * Class CombinationController
 * @package App\Http\Controllers\API
 */

class CombinationAPIController extends AppBaseController
{
    /** @var  CombinationRepository */
    private $combinationRepository;

    /** @var  NumericalModelRepository */
    private $numericalModelRepository;

    public function __construct(CombinationRepository $combinationRepo, NumericalModelRepository $numericalModelRepo)
    {
        $this->combinationRepository = $combinationRepo;
        $this->numericalModelRepository = $numericalModelRepo;
        $this->auth = app()->make('auth');
        // $this->authorizeResource(Combination::class, 'combination');
    }

    public function index(Request $request)
    {
        // $this->combinationRepository->pushCriteria(new RequestCriteria($request));
        // $this->combinationRepository->pushCriteria(new LimitOffsetCriteria($request));
        $affiliation = Auth::user()->affiliation;
        $combinations = $this->combinationRepository->getActiveForAffiliation($affiliation);

        return fractal($combinations, new CombinationTransformer)
                ->withResourceName('combinations')->respond();
    }

    public function store(CreateCombinationAPIRequest $request, DocumentParser $parser)
    {
        $input = $request->all();
        $input['data']['id'] = '[none]';
        $document = $parser->parse(json_encode($input));

        $data = $document->getData();
        $numericalModel = $data->getRelations()['numericalModel']->getIncluded();
        $input = $data->toArray();
        $input['numerical_model_id'] = $this->numericalModelRepository->find($numericalModel->id)->id;
        $input['owner_id'] = $this->auth->user()->id;

        $combination = $this->combinationRepository->create($input);

        return fractal($combination, new CombinationTransformer)
                ->withResourceName('combinations')->respond();
    }

    public function show(Combination $combination)
    {
        return fractal($combination, new CombinationTransformer)
                ->withResourceName('combinations')->respond();
    }

    public function update(Combination $combination, UpdateCombinationAPIRequest $request)
    {
        $input = $request->all();

        $combination = $this->combinationRepository->update($input, $combination->id);

        return fractal($combination, new CombinationTransformer)
                ->withResourceName('combinations')->respond();
    }

    public function destroy(Combination $combination)
    {
        $this->combinationRepository->delete($combination->id);

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
