<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateUserAPIRequest;
use App\Http\Requests\API\UpdateUserAPIRequest;
use App\Models\Users\User;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Transformers\UserTransformer;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Symfony\Component\HttpFoundation\Response;
use Spatie\Fractal\Fractal;

/**
 * Class UserController
 * @package App\Http\Controllers\API
 */

class UserAPIController extends AppBaseController
{
    /** @var  UserRepository */
    private $userRepository;

    public function __construct(UserRepository $userRepo)
    {
        $this->userRepository = $userRepo;
    }

    public function index(Request $request)
    {
        $users = $this->userRepository->all();

        return fractal($users, new UserTransformer)
            ->withResourceName('users')
            ->respond();
    }

    public function store(CreateUserAPIRequest $request)
    {
        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function show($id)
    {
        $transformer = new UserTransformer;

        if ($id == 'current') {
            $user = \Auth::user();
        } else {
            $user = $this->userRepository->find($id);
        }

        if (empty($user)) {
            return $this->sendError('User not found');
        }

        $fields = request()->get('fields');
        if ($fields && array_key_exists('users', $fields)) {
            $additionalFields = explode(',', $fields['users']);
            if (in_array('defaultLocation', $additionalFields)) {
                $transformer->setOption('withDefaultLocation', true);
            }
        }

        return fractal($user, $transformer)
            ->withResourceName('users')
            ->parseIncludes(['affiliation'])
            ->respond();
    }

    public function update($id, UpdateUserAPIRequest $request)
    {
        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function destroy($id)
    {
        return response(null, Response::HTTP_NO_CONTENT);
    }
}
