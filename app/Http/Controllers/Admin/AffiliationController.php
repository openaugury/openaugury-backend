<?php

namespace App\Http\Controllers\Admin;

use Auth;
use A17\Twill\Http\Controllers\Admin\ModuleController;
use Carbon\Carbon;

class AffiliationController extends ModuleController
{
    protected $indexOptions = [
        'create' => true,
        'edit' => true,
        'publish' => true,
        'bulkPublish' => true,
        'feature' => false,
        'bulkFeature' => false,
        'restore' => true,
        'bulkRestore' => true,
        'forceDelete' => true,
        'bulkForceDelete' => true,
        'delete' => true,
        'duplicate' => true,
        'bulkDelete' => true,
        'reorder' => false,
        'permalink' => false, //false to hide this in Add New
        'bulkEdit' => true,
        'editInModal' => false,
    ];

    protected $moduleName = 'affiliations';

    protected $titleColumnKey = 'name';

    protected $perPage = 15;

    protected $indexColumns = [
        'name' => [ // field column
            'title' => 'Name',
            'field' => 'name',
        ],
        'district' => [ // field column
            'title' => 'District',
            'relationship' => 'district',
            'field' => 'name'
        ],
        'created' => [ // field column
            'title' => 'Created',
            'field' => 'formatted_created_at',
        ],
    ];

    // protected function formData($request)
    // {
    //     $simulations = app(\App\Repositories\SimulationRepository::class)->listAll('generated_title');
    //     $challengeTemplates = \App\Models\Interaction\AffiliationTemplate::get()->pluck('text', 'id');
    //     $challengeTypes = [
    //         ['value' => 'writtenWork', 'label' => 'Activity'],
    //         ['value' => 'multiple_choice', 'label' => 'Multiple choice' ]
    //     ];
    //     
    //     $challengeLevels = [
    //         ['value' => 'level1', 'label' => 'Level 1'],
    //         ['value' => 'level2', 'label' => 'Level 2' ]
    //     ];
    //     $futureDateTime = Carbon::now()->addYears(10);

    //     return [
    //         'simulations' => $simulations,
    //         'challengeTypes' => $challengeTypes,
    //         'challengeTemplates' => $challengeTemplates,
    //         'challengeLevels' => $challengeLevels,
    //         'futureDateTime' => $futureDateTime
    //     ];
    // }

    protected function indexData($request)
    {
        $districts = app(\App\Repositories\DistrictRepository::class)->listAll('name');

        return [
            'districts' => $districts
        ];
    }

    protected function formData($request, $item = null)
    {
        $districts = app(\App\Repositories\DistrictRepository::class)->listAll('name');
        $combinations = app(\App\Repositories\CombinationRepository::class)
            ->listAll('phenomenonName')
            ->sort();

        return [
            'districts' => $districts,
            'combinationOptions' => $combinations
        ];
    }
}
