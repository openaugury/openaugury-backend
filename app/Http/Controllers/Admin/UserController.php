<?php

namespace App\Http\Controllers\Admin;

use Auth;
use A17\Twill\Http\Controllers\Admin\ModuleController;
use Carbon\Carbon;

class UserController extends ModuleController
{
    protected $indexOptions = [
        'create' => true,
        'edit' => true,
        'publish' => true,
        'bulkPublish' => true,
        'feature' => false,
        'bulkFeature' => false,
        'restore' => true,
        'bulkRestore' => true,
        'forceDelete' => true,
        'bulkForceDelete' => true,
        'delete' => true,
        'duplicate' => false,
        'bulkDelete' => true,
        'reorder' => false,
        'permalink' => false, //false to hide this in Add New
        'bulkEdit' => true,
        'editInModal' => false,
    ];

    protected $moduleName = 'users';

    protected $titleColumnKey = 'email';

    protected $perPage = 15;

    protected $indexColumns = [
        'forename' => [ // field column
            'title' => 'Forename',
            'field' => 'Forename',
        ],
        'surname' => [ // field column
            'title' => 'Surname',
            'field' => 'surname',
        ],
        'email' => [ // field column
            'title' => 'Email',
            'field' => 'email',
        ],
        'affiliation' => [ // field column
            'title' => 'Affiliation',
            'relationship' => 'affiliation',
            'field' => 'name'
        ],
        'created' => [ // field column
            'title' => 'Created',
            'field' => 'created_at',
        ],
    ];

    // protected function formData($request)
    // {
    //     $simulations = app(\App\Repositories\SimulationRepository::class)->listAll('generated_title');
    //     $challengeTemplates = \App\Models\Interaction\AffiliationTemplate::get()->pluck('text', 'id');
    //     $challengeTypes = [
    //         ['value' => 'writtenWork', 'label' => 'Activity'],
    //         ['value' => 'multiple_choice', 'label' => 'Multiple choice' ]
    //     ];
    //     
    //     $challengeLevels = [
    //         ['value' => 'level1', 'label' => 'Level 1'],
    //         ['value' => 'level2', 'label' => 'Level 2' ]
    //     ];
    //     $futureDateTime = Carbon::now()->addYears(10);

    //     return [
    //         'simulations' => $simulations,
    //         'challengeTypes' => $challengeTypes,
    //         'challengeTemplates' => $challengeTemplates,
    //         'challengeLevels' => $challengeLevels,
    //         'futureDateTime' => $futureDateTime
    //     ];
    // }

    protected function indexData($request)
    {
        $affiliations = app(\App\Repositories\AffiliationRepository::class)->listAll('name');

        return [
            'affiliations' => $affiliations
        ];
    }

    protected function formData($request, $item = null)
    {
        $affiliations = app(\App\Repositories\AffiliationRepository::class)->listAll('name');

        return [
            'affiliations' => $affiliations,
            'currentUser' => \Auth::user()
        ];
    }
}
