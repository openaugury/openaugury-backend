<?php

namespace App\Http\Controllers\Admin;

use Auth;
use A17\Twill\Http\Controllers\Admin\ModuleController;
use Carbon\Carbon;

class InformationalController extends ModuleController
{
    protected $indexOptions = [
        'create' => true,
        'edit' => true,
        'publish' => true,
        'bulkPublish' => true,
        'feature' => false,
        'bulkFeature' => false,
        'restore' => true,
        'bulkRestore' => true,
        'forceDelete' => true,
        'bulkForceDelete' => true,
        'delete' => true,
        'duplicate' => true,
        'bulkDelete' => true,
        'reorder' => false,
        'permalink' => false, //false to hide this in Add New
        'bulkEdit' => true,
        'editInModal' => false,
    ];
                        
    protected $moduleName = 'informationals';

    protected $titleColumnKey = 'text';

    protected $indexColumns = [
        'text' => [ // field column
            'title' => 'Title',
            'field' => 'text',
        ],
        'subtext' => [ // field column
            'title' => 'Description',
            'field' => 'subtext',
            'visible' => false,
        ],
        'url' => [ // field column
            'title' => 'URL',
            'field' => 'url',
        ],
        'time' => [ // field column
            'title' => 'Time',
            'field' => 'time',
        ],
        'informational_type_id' => [ // field column
            'title' => 'Type ID',
            'field' => 'informational_type_id',
        ],
        /*'created' => [ // field column
            'title' => 'Created',
            'field' => 'created_at',
        ],
        'updated' => [ // field column
            'title' => 'Updated',
            'field' => 'updated_at',
        ]*/
    ];

    protected function formData($request)
    {
        $simulations = app(\App\Repositories\SimulationRepository::class)->listAll('generated_title');
        $informationalTemplates = \App\Models\Interaction\InformationalType::get()->pluck('name', 'id');

        $futureDateTime = Carbon::now()->addYears(10);

        return [
            'simulations' => $simulations,
            'informationalTemplates' => $informationalTemplates,
            'futureDateTime' => $futureDateTime
        ];
    }

    protected function indexData($request)
    {
        $simulations = app(\App\Repositories\SimulationRepository::class)->listAll('generated_title');
        $informationalTemplates = \App\Models\Interaction\InformationalType::get()->pluck('name', 'id');
   
        $futureDateTime = Carbon::now()->addYears(10);

        return [
            'simulations' => $simulations,
            'informationalTemplates' => $informationalTemplates,
            'futureDateTime' => $futureDateTime
        ];
    }
}
