<?php

namespace App\Http\Controllers\Passport;

use Illuminate\Contracts\Validation\Factory as ValidationFactory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PersonalAccessTokenController
{
    /**
     * The validation factory implementation.
     *
     * @var \Illuminate\Contracts\Validation\Factory
     */
    protected $validation;

    /**
     * Create a controller instance.
     *
     * @param  \Illuminate\Contracts\Validation\Factory  $validation
     * @return void
     */
    public function __construct(ValidationFactory $validation)
    {
        $this->validation = $validation;
    }

    /**
     * Get all of the personal access tokens for the authenticated user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function forUser(Request $request)
    {
        $tokens = $request->user()->tokens;

        return $tokens;
    }

    /**
     * Create a new personal access token for the user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Laravel\Passport\PersonalAccessTokenResult
     */
    public function store(Request $request)
    {
        $this->validation->make($request->all(), [
            'name' => 'required|max:255',
            'scopes' => 'array'
        ])->validate();

        $token = $request->user()->createToken(
            $request->name, $request->scopes ?: []
        );
        return [
            'token' => $token->accessToken,
            'accessToken' => $token->plainTextToken
        ];
    }

    /**
     * Delete the given token.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $tokenId
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $tokenId)
    {
        $token = $request->user()->tokens()
                                 ->whereId($tokenId)
                                 ->first();

        if (is_null($token)) {
            return new Response('', 404);
        }

        $token->delete();

        return new Response('', Response::HTTP_NO_CONTENT);
    }
}
