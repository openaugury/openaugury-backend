<?php

use Illuminate\Database\Seeder;
use App\Models\Interaction\ChallengeTemplate;

class ChallengeTemplatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $task = ChallengeTemplate::firstOrNew([
            'text' => 'Task',
            'challenge_type' => 'writtenWork',
            'type' => 1
        ]);
        $task->fill([
            'challenge_level' => 'level1',
            'type' => 1,
            'subtext' => '(insert your task description here)',
            'options' => '[]'
        ]);
        $task->save();

        $multichoice = ChallengeTemplate::firstOrNew([
            'text' => 'Multichoice',
            'challenge_type' => 'multiple',
            'type' => 1
        ]);
        $multichoice->fill([
            'type' => 1,
            'challenge_level' => 'level1',
            'subtext' => '(insert your question text here)',
            'options' => json_encode([['value' => "a", 'text' => "First answer"], ['value' => "b", 'text' => "Second answer"], ['value' => "c", 'text' => "Third answer"], ['value' => "d", 'text' => "Fourth answer"]], true),
            'correct' => '"a"'
        ]);
        $multichoice->save();
    }
}
