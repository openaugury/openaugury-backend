<?php

use Phaza\LaravelPostgis\Geometries\LineString;
use Phaza\LaravelPostgis\Geometries\Point;
use Phaza\LaravelPostgis\Geometries\Polygon;
use Illuminate\Database\Seeder;
use App\Models\Features\FeatureSet;

class FeaturesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
      //DB::table('data_sources')->insert([
       // 'name' => 'Dummy Open Data',
       //'owner' => 'Dummy Owner',
       // 'license_title' => 'OGL',
       // 'license_url' => 'http://www.nationalarchives.gov.uk/doc/open-government-licence/version/3/',
       // 'uri' => 'example.org/dummy',
       // 'feature_uri_template' => 'example.org/dummy/%id%/'
      //]);

     // DB::table('feature_types')->insert([
       // 'name' => 'Park',
       // 'slug' => 'park',
       // 'description' => 'Park, field, commons or other public green space',
       // 'symbol' => '\u{1F3DE}'
      //]);


      $feature_location = new Point(54.103, -6.243);
      $feature_extent = new Polygon([new LineString([
          new Point(54.105, -6.242),
          new Point(54.105, -6.244),
          new Point(54.102, -6.246),
          new Point(54.102, -6.243),
          new Point(54.105, -6.242)
      ])]);

     //  $feature = new Feature;
      // $feature->name = 'Clonallon Park';
     // $feature->address = 'Clonallon Park, Warrenpoint, Co Down, Northern Ireland';
     // $feature->slug = 'park-clonallon-park';
     // $feature->description = 'Park in Warrenpoint';
     // $feature->location = $feature_location;
     // $feature->extent = $feature_extent;
     // $feature->wikidata = 'www.openstreetmap.org/way/57741524';
     // $feature->data_source_feature_identifier = '4932-pk-clon-pk-12345';

     // $feature->data_source_id = App\DataSource::whereName('Dummy Open Data')->first()->id;
     // $feature->feature_type_id = App\FeatureType::whereName('Park')->first()->id;

     // $feature->save();
    }
}
