<?php

use Phaza\LaravelPostgis\Geometries\LineString;
use Phaza\LaravelPostgis\Geometries\Point;
use Phaza\LaravelPostgis\Geometries\Polygon;
use GeoJson\GeoJson;
use Illuminate\Database\Seeder;
use Cocur\Slugify\Slugify;
use App\Models\CachedDataServerFeature;
use App\Models\CachedDataServerFeatureSet;
use App\Models\Features\FeatureSet;
use App\Models\Features\District;

class DrainageAssetsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $dataSource = CachedDataServerFeatureSet::whereName('Drainage Assets')->first();

        if ($dataSource) {
            $dataSource->cachedDataServerFeatures()->delete();
        } else {
            $dataSource = CachedDataServerFeatureSet::create([
			          'name' => 'Drainage Assets',
				        'owner' => 'Department for Infrastructure - Roads',
					      'license_title' => 'UK-OGL',
						    'license_url' => 'http://www.nationalarchives.gov.uk/doc/open-government-licence/version/3/',
							  'uri' => 'https://www.opendatani.gov.uk/dataset/drainage-asset',
								'data_server' => 'opendatani',
								'data_server_set_id' => 'drainage-assets'
            ]);
        }

        $district = District::whereName('Northern Ireland 1')->first();
        $featureType = FeatureSet::whereSlug('drainage-assets')->first();
        $featureType->districts()->sync([
            $district->id => [
                'data_server_set_id' => $dataSource->data_server_set_id,
                'data_server' => 'opendatani',
                'status' => 0
            ]
        ]);
				
				$drainageAssets = ['belfastnorthgullies.geojson'];
				foreach($drainageAssets as $drainageAsset){
						$drainageDataJson = json_decode(file_get_contents(base_path() . '/resources/opendata/' . $drainageAsset));
		        $counter = 0;
				    $drainage = GeoJson::jsonUnserialize($drainageDataJson);
		        $counter_total = count($drainage);
						//** broken-for-loop
						// for($loadedAssets = 0; $loadedAssets<count($drainage); $loadedAssets+=200){
								// $drainageAsset = $drainage[$loadedAssets];
						foreach($drainage as $drainageAsset){
								$coordinates = $drainageAsset->getGeometry()->getCoordinates();
		            $feature = new CachedDataServerFeature;
				        $properties = $drainageAsset->getProperties();
						    $feature->feature_id = $properties['SECTION_NAME'];
								$feature->location = new Point($coordinates[1], $coordinates[0]);
		            $feature->json = json_encode($drainageAsset);
						    $feature->cached_data_server_feature_set_id = $dataSource->id;
								$feature->save();
		
								$counter++;
				        if ($counter % 1000 == 0) {
						        // $this->command->info("{$drainageAsset}: Added {$counter}/{$counter_total} gullies");
								}
						}
					}
				}
}
