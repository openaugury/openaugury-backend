<?php

use Phaza\LaravelPostgis\Geometries\LineString;
use Phaza\LaravelPostgis\Geometries\Point;
use Phaza\LaravelPostgis\Geometries\Polygon;
use GeoJson\GeoJson;
use Illuminate\Database\Seeder;
use Cocur\Slugify\Slugify;
use App\Models\CachedDataServerFeature;
use App\Models\CachedDataServerFeatureSet;
use App\Models\Features\FeatureSet;
use App\Models\Features\District;

class SA_RiyadhSchoolsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dataSource = CachedDataServerFeatureSet::whereDataServerSetId('fat-saudi-arabia-schools')->first();

        if ($dataSource) {
            $dataSource->cachedDataServerFeatures()->delete();
        } else {
            $dataSource = CachedDataServerFeatureSet::create([
              'name' => 'Saudi Arabia Riyadh Schools',
              'owner' => 'Department of Communities',
              'license_title' => 'UK-OGL',
              'license_url' => 'http://www.nationalarchives.gov.uk/doc/open-government-licence/version/3/',
              'uri' => 'https://www.opendatani.gov.uk/dataset/areas-of-archaeological-potential',
              'data_server' => 'orp-ckan',
              'data_server_set_id' => 'fat-saudi-arabia-schools'
            ]);
        }

        $archPotJson = json_decode(file_get_contents(base_path() . '/resources/opendata/schools_riyadh.geojson'));

        $archPots = GeoJson::jsonUnserialize($archPotJson);

        foreach ($archPots as $archPot) {


            $type = $archPot->getGeometry()->getType(); 
            if ($type == 'Polygon') {
                $coordinates = $archPot->getGeometry()->getCoordinates()[0][0];
            }
            else {
                $coordinates = $archPot->getGeometry()->getCoordinates();
            }
            $feature = new CachedDataServerFeature;
            $properties = $archPot->getProperties();
            $decodedarchPot = json_decode(json_encode($archPot),true);
            $feature->feature_id = $properties['@id'];
            $feature->location = new Point($coordinates[1], $coordinates[0]);
            $amenity = ucwords($properties['amenity']);
            if (!array_key_exists('name',$properties)) {
                if (array_key_exists('{name:en',$properties)) {
                    $decodedarchPot["properties"]['name'] = $properties['name:en'];
                }
                else {
                    $decodedarchPot["properties"]['name'] = $amenity;
                }
            }
            $decodedarchPot["properties"]['description'] = $amenity;
            $feature->json = json_encode($decodedarchPot);
            $feature->cached_data_server_feature_set_id = $dataSource->id;
            
            $feature->save();
        }
    }
}
