<?php

use App\Models\Users\User;
use App\Models\Users\Affiliation;
use App\Models\Features\District;
use App\Models\AbstractTier\Phenomenon;
use Illuminate\Database\Seeder;

class SampleUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //$users = factory(User::class, 10)->create();
        $affiliation = Affiliation::firstOrCreate(
            [
                'name' => 'Example School'
            ]
        );
        $affiliation->district_id = District::where([
            'slug' => 'sa-1'
        ])->firstOrFail()->id;
        $affiliation->save();

        $affiliation2 = Affiliation::firstOrCreate(
            [
                'name' => 'Example School 2'
            ]
        );
        $affiliation2->district_id = District::where([
            'slug' => 'sa-2'
        ])->firstOrFail()->id;
        $affiliation2->save();
        
        $affiliation3 = Affiliation::firstOrCreate(
            [
                'name' => 'Example School 3'
            ]
        );
        $affiliation3->district_id = District::where([
            'slug' => 'sa-3'
        ])->firstOrFail()->id;
        $affiliation3->save();
        
        $affiliation4 = Affiliation::firstOrCreate(
            [
                'name' => 'Example School 4'
            ]
        );
        $affiliation4->district_id = District::where([
            'name' => 'Northern Ireland 1'
        ])->firstOrFail()->id;
        $affiliation4->save();

        $teacher = User::firstOrNew(
            [
                'forename' => 'Test',
                'surname' => 'Teacher',
                'email' => 'testteach@example.com',
            ]
        );
        $teacher['password'] = 'secret';
        $teacher['identifier'] = 'teacher';
        $teacher['affiliation_id'] = $affiliation->id;

        $teacher->save();
        $teacher->assignRole('teacher');
        $teacher->save();

        $teacher = User::firstOrNew(
            [
                'forename' => 'Test',
                'surname' => 'Teacher',
                'email' => 'testteach2@example.com',
            ]
        );
        $teacher['password'] = 'secret2';
        $teacher['identifier'] = 'teacher102';
        $teacher['affiliation_id'] = $affiliation2->id;

        $teacher->save();
        $teacher->assignRole('teacher');
        $teacher->save();

        $teacher = User::firstOrNew(
            [
                'forename' => 'TestDhahran',
                'surname' => 'Teacher',
                'email' => 'testteach3@example.com',
            ]
        );
        $teacher['password'] = 'secret3';
        $teacher['identifier'] = 'teacher103';
        $teacher['affiliation_id'] = $affiliation3->id;

        $teacher->save();
        $teacher->assignRole('teacher');
        $teacher->save();

        $teacher = User::firstOrNew(
            [
                'forename' => 'TestNI',
                'surname' => 'Teacher',
                'email' => 'testteach4@example.com',
            ]
        );
        $teacher['password'] = 'secret4';
        $teacher['identifier'] = 'teacher104';
        $teacher['affiliation_id'] = $affiliation4->id;

        $teacher->save();
        $teacher->assignRole('teacher');
        $teacher->save();

        $stormSurge = Phenomenon::whereName('Hurricane Storm Surge')->first();
        $combination = $stormSurge->numericalModels()->first()->combinations()->first();
        $affiliation->combinations()->attach($combination);
        $affiliation2->combinations()->attach($combination);
        $affiliation3->combinations()->attach($combination);
        $affiliation4->combinations()->attach($combination);
    }
}
