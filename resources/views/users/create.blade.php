@extends('panelViews::master')
@section('bodyClass')
register
@stop
@section('body')
    <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    
                    <div class="login-panel panel panel-default">
                     
                        <div class="panel-body">
                            <div class="logo-holder">
                                <img src="{{asset(Config::get('panel.logo'))}}" />
                            </div>
                            {!! Form::open(array('route' => 'users.store')) !!}
                                <fieldset>
                                    <div class="form-group">
                                        <label for="district_id">{{ _i('District:') }} </label>
                                        <select class="form-control" placeholder="District ID" name="district_id" type="text" autofocus>
                                            @foreach ($districts as $district)
                                                <option value="{{ $district->id }}">{{ $district->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="affiliation_name">{{ _i('Affiliation Name:') }} </label>
                                        <input class="form-control" placeholder="Affiliation Name" name="affiliation_name" type="text" autofocus>
                                    </div>
                                    <div class="form-group">
                                        <label for="first_name">{{ _i('First Name:') }}</label>
                                        <input class="form-control" placeholder="First Name" name="first_name" type="text" autofocus>
                                    </div>
                                    <div class="form-group">
                                        <label for="first_name">{{ _i('Last Name:') }}</label>
                                        <input class="form-control" placeholder="Last Name" name="last_name" type="text" autofocus>
                                    </div>
                                    <div class="form-group">
                                        <label for="first_name">{{ _i('Email:') }}</label>
                                        <input class="form-control" placeholder="{{ \Lang::get('panel::fields.email') }}" name="email" type="text" autofocus>
                                    </div>
                                    <div class="form-group">
                                        <label for="first_name">{{ _i('Password:') }}</label>
                                        <input class="form-control" placeholder="{{ \Lang::get('panel::fields.password') }}" name="password" type="password" value="">
                                    </div>
                                    <!-- Change this to a button or input when using this as a form -->
                                    <input type="submit"  class="btn btn-lg btn-success btn-block" value="Register">
                                </fieldset>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
    </div>
@stop

