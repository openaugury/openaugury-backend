<table class="table table-responsive" id="resultAnalytics-table">
    <thead>
        <th>{{ _i('Analytic Id') }}</th>
        <th>{{ _i('Simulation Id') }}</th>
        <th>{{ _i('Value') }}</th>
        <th colspan="3">{{ _i('Action') }}</th>
    </thead>
    <tbody>
    @foreach($resultAnalytics as $resultAnalytic)
        <tr>
            <td>{!! $resultAnalytic->analytic_id !!}</td>
            <td>{!! $resultAnalytic->simulation_id !!}</td>
            <td>{!! $resultAnalytic->value !!}</td>
            <td>
                {!! Form::open(['route' => ['resultAnalytics.destroy', $resultAnalytic->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('resultAnalytics.show', [$resultAnalytic->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('resultAnalytics.edit', [$resultAnalytic->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>