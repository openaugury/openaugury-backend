<!-- Text Field -->
<div class="form-group col-sm-6">
    {!! Form::label('text', 'Text:') !!}
    {!! Form::text('text', null, ['class' => 'form-control']) !!}
</div>

<!-- Subtext Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('subtext', 'Subtext:') !!}
    {!! Form::textarea('subtext', null, ['class' => 'form-control']) !!}
</div>

<!-- Options Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('options', 'Options:') !!}
    {!! Form::textarea('options', null, ['class' => 'form-control']) !!}
</div>

<!-- Correct Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('correct', 'Correct:') !!}
    {!! Form::textarea('correct', null, ['class' => 'form-control']) !!}
</div>

<!-- Mark Field -->
<div class="form-group col-sm-6">
    {!! Form::label('mark', 'Mark:') !!}
    {!! Form::number('mark', null, ['class' => 'form-control']) !!}
</div>

<!-- Challenge Template Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('challenge_template_id', 'Challenge Template Id:') !!}
    {!! Form::number('challenge_template_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Case Context Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('case_context_id', 'Case Context Id:') !!}
    {!! Form::text('case_context_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Result Analytic Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('result_analytic_id', 'Result Analytic Id:') !!}
    {!! Form::number('result_analytic_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('challenges.index') !!}" class="btn btn-default">{{ _i('Cancel') }}</a>
</div>
