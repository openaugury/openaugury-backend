@extends('twill::layouts.form')

@push('extra_css')
<link rel="stylesheet" href="//unpkg.com/leaflet/dist/leaflet.css" />
@endpush

@push('extra_js_head')
<script src="//unpkg.com/leaflet/dist/leaflet.js"></script>
<script src="//unpkg.com/vue2-leaflet"></script>
@endpush

@section('contentFields')
    @formField('input', [
        'name' => 'name',
        'label' => 'Name',
        'maxlength' => 100
    ])


    @formField('checkboxes', [
       'name' => 'activeFeatureChunks',
       'label' => 'Feature Sets',
       'inline' => true,
       'options' => $caseFeatureChunksOptions
    ])

    @formField('select', [
        'name' => 'district_id',
        'label' => 'District',
        'placeholder' => 'Select a district',
        'options' => $districts,
        'maxlength' => 100
    ])

    @formField('osmap', [
       'name' => 'centerLatLng',
       'disabled' => $item->simulations->count(),
       'label' => 'Centre',
       'placeholder' => 'Select epicentre',
       'initialLocation' => [54.6, '-5.92'],
       'initialZoom' => 15
    ])

   @formField('date_picker', [
    'name' => 'begins',
    'label' => 'Beginning Time',
    //'minDate' => '2017-09-10 12:00',
    //'maxDate' => '2017-12-10 12:00'
    ])

    @formField('date_picker', [
    'name' => 'ends',
    'label' => 'Ending Time',
    //'minDate' => '2017-09-10 12:00',
    //'maxDate' => '2017-12-10 12:00'
    ])
@stop
