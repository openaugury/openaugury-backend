@formField('input', [
    'name' => 'text',
    'label' => 'Name',
    'placeholder' => 'Enter name'
])

@formField('select', [
    'name' => 'district_id',
    'label' => 'District',
    'placeholder' => 'Select a District',
    'options' => $districts
])

