@extends('twill::layouts.form')

@push('extra_css')
<link rel="stylesheet" href="//unpkg.com/leaflet/dist/leaflet.css" />
@endpush

@push('extra_js_head')
<script src="//unpkg.com/leaflet/dist/leaflet.js"></script>
<script src="//unpkg.com/vue2-leaflet"></script>
@endpush

@section('contentFields')
@formField('select', [
    'name' => 'district_id',
    'label' => 'District',
    'placeholder' => 'Select a district',
    'options' => $districts,
    'maxlength' => 100
])

@formField('input', [
    'name' => 'name',
    'label' => 'Name',
    'placeholder' => '(name)',
    'note' => 'Please enter name of this organization/institution/school.',
    'maxlength' => 200
])

@formField('osmap', [
   'name' => 'locationLatLng',
   'label' => 'Location',
   'placeholder' => 'Select location',
   'initialLocation' => [54.6, '-5.92'],
   'initialZoom' => 15
])

@formField('checkboxes', [
   'name' => 'activeCombinations',
   'label' => 'Models',
   'inline' => false,
   'options' => $combinationOptions
])
@stop
