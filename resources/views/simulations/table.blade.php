<table class="table table-responsive" id="simulations-table">
    <thead>
        <th>{{ _i('Case Context Id') }}</th>
        <th>{{ _i('Combination Id') }}</th>
        <th>{{ _i('Settings') }}</th>
        <th>{{ _i('Definition') }}</th>
        <th>{{ _i('Status') }}</th>
        <th>{{ _i('Center') }}</th>
        <th>{{ _i('Extent') }}</th>
        <th>{{ _i('Begins') }}</th>
        <th>{{ _i('Ends') }}</th>
        <th>{{ _i('Result') }}</th>
        <th>{{ _i('Owner Id') }}</th>
        <th colspan="3">{{ _i('Action') }}</th>
    </thead>
    <tbody>
    @foreach($simulations as $simulation)
        <tr>
            <td>{!! $simulation->case_context_id !!}</td>
            <td>{!! $simulation->combination_id !!}</td>
            <td>{!! $simulation->settings !!}</td>
            <td>{!! $simulation->definition !!}</td>
            <td>{!! $simulation->status !!}</td>
            <td>{!! $simulation->center !!}</td>
            <td>{!! $simulation->extent !!}</td>
            <td>{!! $simulation->begins !!}</td>
            <td>{!! $simulation->ends !!}</td>
            <td>{!! $simulation->result !!}</td>
            <td>{!! $simulation->owner_id !!}</td>
            <td>
                {!! Form::open(['route' => ['simulations.destroy', $simulation->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('simulations.show', [$simulation->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('simulations.edit', [$simulation->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>