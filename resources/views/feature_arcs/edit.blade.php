@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
        {{ _i('Feature Arc') }}
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($featureArc, ['route' => ['featureArcs.update', $featureArc->id], 'method' => 'patch']) !!}

                        @include('feature_arcs.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection