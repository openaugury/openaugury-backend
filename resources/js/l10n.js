const LOCALIZED_ASSETS = {
  'logo-text.png': {
    locales: {},
    map: 'logo-text-LOCALE.png'
  }
};

function assetl10n (asset, locale) {
  if (asset in LOCALIZED_ASSETS && locale in LOCALIZED_ASSETS[asset].locales) {
    const localeString = LOCALIZED_ASSETS[asset].locales[locale];
    return LOCALIZED_ASSETS[asset].map.replace('LOCALE', localeString);
  }
  return asset;
}

export default { assetl10n };
