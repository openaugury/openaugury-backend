<!-- Status Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('status', 'Status:'); ?>

    <?php echo Form::number('status', null, ['class' => 'form-control']); ?>

</div>

<!-- Numerical Model Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('numerical_model_id', 'Numerical Model Id:'); ?>

    <?php echo Form::text('numerical_model_id', null, ['class' => 'form-control']); ?>

</div>

<!-- Owner Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('owner_id', 'Owner Id:'); ?>

    <?php echo Form::text('owner_id', null, ['class' => 'form-control']); ?>

</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <?php echo Form::submit('Save', ['class' => 'btn btn-primary']); ?>

    <a href="<?php echo route('combinations.index'); ?>" class="btn btn-default">Cancel</a>
</div>
