<!-- Resource Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('resource_id', 'Resource Id:'); ?>

    <?php echo Form::number('resource_id', null, ['class' => 'form-control']); ?>

</div>

<!-- Simulation Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('simulation_id', 'Simulation Id:'); ?>

    <?php echo Form::text('simulation_id', null, ['class' => 'form-control']); ?>

</div>

<!-- Quantity Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('quantity', 'Quantity:'); ?>

    <?php echo Form::number('quantity', null, ['class' => 'form-control']); ?>

</div>

<!-- Duration Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('duration', 'Duration:'); ?>

    <?php echo Form::number('duration', null, ['class' => 'form-control']); ?>

</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <?php echo Form::submit('Save', ['class' => 'btn btn-primary']); ?>

    <a href="<?php echo route('resultResources.index'); ?>" class="btn btn-default">Cancel</a>
</div>
