<?php
/**
 * This file, unlike most of the code in this codebase, is under Apache 2.0 license.
 * https://www.apache.org/licenses/LICENSE-2.0
 * We gratefully acknowledge the work of the Area17/Twill team in creating
 * the original LocationField.vue
 */
    $showMap = $showMap ?? true;
    $openMap = $openMap ?? false;
    $inModal = $fieldsInModal ?? false;
    $initialLocation = $initialLocation ?? [0, 0];
    $initialZoom = $initialZoom ?? 9;
    $disabled = $disabled ?? false;
?>

<a17-oslocationfield
    label="<?php echo e($label); ?>"
    <?php if($disabled): ?> disabled <?php endif; ?>
    <?php echo $__env->make('twill::partials.form.utils._field_name', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php if($inModal): ?> :in-modal="true" <?php endif; ?>
    initialLat="<?php echo e($initialLocation[0]); ?>"
    initialLng="<?php echo e($initialLocation[1]); ?>"
    zoom="<?php echo e($initialZoom); ?>"
    in-store="value"
></a17-oslocationfield>

<?php if (! ($renderForBlocks || $renderForModal || !isset($item->$name))): ?>
<?php $__env->startPush('vuexStore'); ?>
    window['<?php echo e(config('twill.js_namespace')); ?>'].STORE.form.fields.push({
        name: '<?php echo e($name); ?>',
        value: <?php echo json_encode($item->$name); ?>

    })
<?php $__env->stopPush(); ?>
<?php endif; ?>
