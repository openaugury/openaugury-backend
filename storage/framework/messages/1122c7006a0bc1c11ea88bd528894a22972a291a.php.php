<!-- Id Field -->
<div class="form-group">
    <?php echo Form::label('id', 'Id:'); ?>

    <p><?php echo $caseContext->id; ?></p>
</div>

<!-- District Id Field -->
<div class="form-group">
    <?php echo Form::label('district_id', 'District Id:'); ?>

    <p><?php echo $caseContext->district_id; ?></p>
</div>

<!-- Name Field -->
<div class="form-group">
    <?php echo Form::label('name', 'Name:'); ?>

    <p><?php echo $caseContext->name; ?></p>
</div>

<!-- Center Field -->
<div class="form-group">
    <?php echo Form::label('center', 'Center:'); ?>

    <p><?php echo $caseContext->center; ?></p>
</div>

<!-- Extent Field -->
<div class="form-group">
    <?php echo Form::label('extent', 'Extent:'); ?>

    <p><?php echo $caseContext->extent; ?></p>
</div>

<!-- Begins Field -->
<div class="form-group">
    <?php echo Form::label('begins', 'Begins:'); ?>

    <p><?php echo $caseContext->begins; ?></p>
</div>

<!-- Ends Field -->
<div class="form-group">
    <?php echo Form::label('ends', 'Ends:'); ?>

    <p><?php echo $caseContext->ends; ?></p>
</div>

<!-- Owner Id Field -->
<div class="form-group">
    <?php echo Form::label('owner_id', 'Owner Id:'); ?>

    <p><?php echo $caseContext->owner_id; ?></p>
</div>

<!-- Created At Field -->
<div class="form-group">
    <?php echo Form::label('created_at', 'Created At:'); ?>

    <p><?php echo $caseContext->created_at; ?></p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    <?php echo Form::label('updated_at', 'Updated At:'); ?>

    <p><?php echo $caseContext->updated_at; ?></p>
</div>

