<table class="table table-responsive" id="informationals-table">
    <thead>
        <th>Text</th>
        <th>Subtext</th>
        <th>Informational Type Id</th>
        <th>Case Context Id</th>
        <th>Creator Id</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $informationals; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $informational): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $informational->text; ?></td>
            <td><?php echo $informational->subtext; ?></td>
            <td><?php echo $informational->informational_type_id; ?></td>
            <td><?php echo $informational->case_context_id; ?></td>
            <td><?php echo $informational->creator_id; ?></td>
            <td>
                <?php echo Form::open(['route' => ['informationals.destroy', $informational->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('informationals.show', [$informational->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('informationals.edit', [$informational->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>