<!-- Cached Data Server Feature Set Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('cached_data_server_feature_set_id', 'Cached Data Server Feature Set Id:'); ?>

    <?php echo Form::number('cached_data_server_feature_set_id', null, ['class' => 'form-control']); ?>

</div>

<!-- Feature Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('feature_id', 'Feature Id:'); ?>

    <?php echo Form::text('feature_id', null, ['class' => 'form-control']); ?>

</div>

<!-- Location Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('location', 'Location:'); ?>

    <?php echo Form::text('location', null, ['class' => 'form-control']); ?>

</div>

<!-- Extent Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('extent', 'Extent:'); ?>

    <?php echo Form::text('extent', null, ['class' => 'form-control']); ?>

</div>

<!-- Json Field -->
<div class="form-group col-sm-12 col-lg-12">
    <?php echo Form::label('json', 'Json:'); ?>

    <?php echo Form::textarea('json', null, ['class' => 'form-control']); ?>

</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <?php echo Form::submit('Save', ['class' => 'btn btn-primary']); ?>

    <a href="<?php echo route('cachedDataServerFeatures.index'); ?>" class="btn btn-default">Cancel</a>
</div>
