<?php $__env->startSection('bodyClass'); ?>
dashboard
<?php $__env->stopSection(); ?>
<?php $__env->startSection('body'); ?>


<?php if(Session::has('message')): ?>
    <div class="alert-box success">
        <h2><?php echo e(Session::get('message')); ?></h2>
    </div>
<?php endif; ?>
   

<?php $__env->stopSection(); ?>
<?php echo $__env->make('panelViews::master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>