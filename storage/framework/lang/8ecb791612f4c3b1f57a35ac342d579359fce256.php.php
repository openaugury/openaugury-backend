<table class="table table-responsive" id="numericalModels-table">
    <thead>
        <th>Name</th>
        <th>Definition</th>
        <th>Phenomenon Id</th>
        <th>Owner Id</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $numericalModels; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $numericalModel): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $numericalModel->name; ?></td>
            <td><?php echo $numericalModel->definition; ?></td>
            <td><?php echo $numericalModel->phenomenon_id; ?></td>
            <td><?php echo $numericalModel->owner_id; ?></td>
            <td>
                <?php echo Form::open(['route' => ['numericalModels.destroy', $numericalModel->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('numericalModels.show', [$numericalModel->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('numericalModels.edit', [$numericalModel->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>