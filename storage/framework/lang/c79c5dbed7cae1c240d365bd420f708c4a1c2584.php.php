<!-- Analytic Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('analytic_id', 'Analytic Id:'); ?>

    <?php echo Form::number('analytic_id', null, ['class' => 'form-control']); ?>

</div>

<!-- Simulation Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('simulation_id', 'Simulation Id:'); ?>

    <?php echo Form::text('simulation_id', null, ['class' => 'form-control']); ?>

</div>

<!-- Value Field -->
<div class="form-group col-sm-12 col-lg-12">
    <?php echo Form::label('value', 'Value:'); ?>

    <?php echo Form::textarea('value', null, ['class' => 'form-control']); ?>

</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <?php echo Form::submit('Save', ['class' => 'btn btn-primary']); ?>

    <a href="<?php echo route('resultAnalytics.index'); ?>" class="btn btn-default">Cancel</a>
</div>
