<table class="table table-responsive" id="resultAnalytics-table">
    <thead>
        <th>Analytic Id</th>
        <th>Simulation Id</th>
        <th>Value</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $resultAnalytics; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $resultAnalytic): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $resultAnalytic->analytic_id; ?></td>
            <td><?php echo $resultAnalytic->simulation_id; ?></td>
            <td><?php echo $resultAnalytic->value; ?></td>
            <td>
                <?php echo Form::open(['route' => ['resultAnalytics.destroy', $resultAnalytic->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('resultAnalytics.show', [$resultAnalytic->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('resultAnalytics.edit', [$resultAnalytic->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>