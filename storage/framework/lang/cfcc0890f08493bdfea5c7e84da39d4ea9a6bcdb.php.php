<!-- Id Field -->
<div class="form-group">
    <?php echo Form::label('id', 'Id:'); ?>

    <p><?php echo $simulation->id; ?></p>
</div>

<!-- Case Context Id Field -->
<div class="form-group">
    <?php echo Form::label('case_context_id', 'Case Context Id:'); ?>

    <p><?php echo $simulation->case_context_id; ?></p>
</div>

<!-- Combination Id Field -->
<div class="form-group">
    <?php echo Form::label('combination_id', 'Combination Id:'); ?>

    <p><?php echo $simulation->combination_id; ?></p>
</div>

<!-- Settings Field -->
<div class="form-group">
    <?php echo Form::label('settings', 'Settings:'); ?>

    <p><?php echo $simulation->settings; ?></p>
</div>

<!-- Definition Field -->
<div class="form-group">
    <?php echo Form::label('definition', 'Definition:'); ?>

    <p><?php echo $simulation->definition; ?></p>
</div>

<!-- Status Field -->
<div class="form-group">
    <?php echo Form::label('status', 'Status:'); ?>

    <p><?php echo $simulation->status; ?></p>
</div>

<!-- Center Field -->
<div class="form-group">
    <?php echo Form::label('center', 'Center:'); ?>

    <p><?php echo $simulation->center; ?></p>
</div>

<!-- Extent Field -->
<div class="form-group">
    <?php echo Form::label('extent', 'Extent:'); ?>

    <p><?php echo $simulation->extent; ?></p>
</div>

<!-- Begins Field -->
<div class="form-group">
    <?php echo Form::label('begins', 'Begins:'); ?>

    <p><?php echo $simulation->begins; ?></p>
</div>

<!-- Ends Field -->
<div class="form-group">
    <?php echo Form::label('ends', 'Ends:'); ?>

    <p><?php echo $simulation->ends; ?></p>
</div>

<!-- Result Field -->
<div class="form-group">
    <?php echo Form::label('result', 'Result:'); ?>

    <p><?php echo $simulation->result; ?></p>
</div>

<!-- Owner Id Field -->
<div class="form-group">
    <?php echo Form::label('owner_id', 'Owner Id:'); ?>

    <p><?php echo $simulation->owner_id; ?></p>
</div>

<!-- Created At Field -->
<div class="form-group">
    <?php echo Form::label('created_at', 'Created At:'); ?>

    <p><?php echo $simulation->created_at; ?></p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    <?php echo Form::label('updated_at', 'Updated At:'); ?>

    <p><?php echo $simulation->updated_at; ?></p>
</div>

