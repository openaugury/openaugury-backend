<?php $__env->startSection('page-wrapper'); ?>

<?php if(!empty($message)): ?>
    <div class="alert-box success">
        <h2><?php echo e($message); ?></h2>
    </div>
<?php endif; ?>

<div class="row">
    <div class="col-xs-4" >
      <div class="well well-lg">
        <?php echo Form::model($admin, array( $admin->id)); ?>


        <?php echo Form::label('first_name', \Lang::get('panel::fields.FirstName')); ?>

        <?php echo Form::text('first_name', $admin->first_name, array('class' => 'form-control')); ?>

        <br />
        <?php echo Form::label('last_name', \Lang::get('panel::fields.LastName')); ?>

        <?php echo Form::text('last_name', $admin->last_name, array('class' => 'form-control')); ?>

        <br />
        <!-- email -->
        <?php echo Form::label('email', \Lang::get('panel::fields.email')); ?>

        <?php echo Form::email('email', $admin->email, array('class' => 'form-control')); ?>

        <br />
        <?php echo Form::submit(\Lang::get('panel::fields.updateProfile'), array('class' => 'btn btn-primary')); ?>


        <?php echo Form::close(); ?>

      </div>

  </div>
</div>

<?php if(is_numeric($admin->affiliation->canSimulate())): ?>
<div class="row">
    <div class="col-xs-4" >
      <div class="well well-lg">
        <!-- Remaining credits -->
          <?php echo e(Form::label('affiliation_simulations_remaining', 'Simulations remaining for ' . $admin->affiliation->name)); ?>:

          <?php echo e($admin->affiliation->simulations_remaining); ?>

        <br />
        <p>Simulations are metered. Certain services, which are being expanded, may be temporarily exempt from paid metering; when stabilised, they will be metered in line with existing Pricing documentation.</p>
        <?php echo e(Form::open(['url' => '/affiliations/' . $admin->affiliation->id . '/add-simulations-30', 'method' => 'POST'])); ?>

          <script
            src="https://checkout.stripe.com/checkout.js" class="stripe-button"
            data-key="pk_test_JGFfP8reY3y9GDbZ2CAqay4X"
            data-amount="9000"
            data-name="Flax &amp; Teal Limited"
            data-label="Add 30 simulations"
            data-description="30 simulations"
            data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
            data-locale="auto"
            data-zip-code="true"
            data-currency="gbp">
          </script>
        <?php echo e(Form::close()); ?>

        </form>
      </div>
  </div>
</div>
<?php endif; ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('panelViews::mainTemplate', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>