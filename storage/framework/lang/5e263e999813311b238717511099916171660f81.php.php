<?php $__env->startSection('bodyClass'); ?>
register
<?php $__env->stopSection(); ?>
<?php $__env->startSection('body'); ?>
    <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    
                    <div class="login-panel panel panel-default">
                     
                        <?php echo $__env->make('flash::message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

                        <div class="panel-body">
                            <div class="logo-holder">
                                <img src="<?php echo e(asset(Config::get('panel.logo'))); ?>" />
                            </div>

                            <?php if(count($errors)): ?>
                                <ul>
                                    <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li><?php echo e($error); ?></li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            <?php endif; ?>

                            <h1>Non-Academic User Registration</h1>

                            <?php echo Form::open(array('route' => 'users.store-register')); ?>

                                <fieldset>
                                    <div class="form-group">
                                        <label for="affiliation_code">Join existing Affiliation with access code: </label>
                                        <input class="form-control" placeholder="Affiliation Code" name="affiliation_code" type="text" autofocus>
                                    </div>
                                    <div class="form-group">
                                        <label for="affiliation_name">or create Affiliation with name: </label>
                                        <input class="form-control" placeholder="Affiliation Name" name="affiliation_name" type="text" autofocus>
                                    </div>
                                    <hr/>
                                    <div class="form-group">
                                        <label for="forename">First Name:</label>
                                        <input class="form-control" placeholder="First Name" name="forename" type="text" autofocus>
                                    </div>
                                    <div class="form-group">
                                        <label for="surname">Last Name:</label>
                                        <input class="form-control" placeholder="Last Name" name="surname" type="text" autofocus>
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Email:</label>
                                        <input class="form-control" placeholder="<?php echo e(\Lang::get('panel::fields.email')); ?>" name="email" type="text" autofocus>
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Password:</label>
                                        <input class="form-control" placeholder="<?php echo e(\Lang::get('panel::fields.password')); ?>" name="password" type="password" value="">
                                    </div>
                                    <div class="form-group">
                                        <label for="password_confirmation">Confirm Password:</label>
                                        <input class="form-control" placeholder="<?php echo e(\Lang::get('panel::fields.password')); ?>" name="password_confirmation" type="password" value="">
                                    </div>
                                    <hr/>
                                    <div class="form-group">
                                        <label for="district_id">Data District: </label>
                                        <select class="form-control" placeholder="District ID" name="district_id" type="text" autofocus>
                                            <?php $__currentLoopData = $districts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $district): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($district->id); ?>"><?php echo e($district->name); ?></option>
                                                <option disabled>Contact us to request unlisted districts</option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                        <p>Data districts will be progressively expanded, but if you cannot see your required district above, please contact us to explore expedition options.</p>
                                    </div>
                                    <hr/>
                                    <div class="form-group">
                                        <label for="agreement">I agree and have authority to agree to the <a href="<?php echo e(config('openaugury.legal.terms-and-conditions-link')); ?>">Terms and Conditions</a> and <a href="<?php echo e(config('openaugury.legal.privacy-policy-link')); ?>">Privacy Policy</a> on behalf of my affiliation:</label>
                                        <input class="form-control" placeholder="I agree to terms and conditions, and privacy policy" name="agreement" type="checkbox" value="1">
                                    </div>
                                    <!-- Change this to a button or input when using this as a form -->
                                    <input type="submit"  class="btn btn-lg btn-success btn-block" value="Register">
                                </fieldset>
                            <?php echo Form::close(); ?>

                        </div>
                    </div>
                </div>
            </div>
    </div>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('panelViews::master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>