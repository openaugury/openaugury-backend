<table class="table table-responsive" id="simulations-table">
    <thead>
        <th>Case Context Id</th>
        <th>Combination Id</th>
        <th>Settings</th>
        <th>Definition</th>
        <th>Status</th>
        <th>Center</th>
        <th>Extent</th>
        <th>Begins</th>
        <th>Ends</th>
        <th>Result</th>
        <th>Owner Id</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $simulations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $simulation): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $simulation->case_context_id; ?></td>
            <td><?php echo $simulation->combination_id; ?></td>
            <td><?php echo $simulation->settings; ?></td>
            <td><?php echo $simulation->definition; ?></td>
            <td><?php echo $simulation->status; ?></td>
            <td><?php echo $simulation->center; ?></td>
            <td><?php echo $simulation->extent; ?></td>
            <td><?php echo $simulation->begins; ?></td>
            <td><?php echo $simulation->ends; ?></td>
            <td><?php echo $simulation->result; ?></td>
            <td><?php echo $simulation->owner_id; ?></td>
            <td>
                <?php echo Form::open(['route' => ['simulations.destroy', $simulation->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('simulations.show', [$simulation->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('simulations.edit', [$simulation->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>