<!-- First Field -->
<div class="form-group col-sm-12 col-lg-12">
    <?php echo Form::label('first', 'First:'); ?>

    <?php echo Form::textarea('first', null, ['class' => 'form-control']); ?>

</div>

<!-- Final Field -->
<div class="form-group col-sm-12 col-lg-12">
    <?php echo Form::label('final', 'Final:'); ?>

    <?php echo Form::textarea('final', null, ['class' => 'form-control']); ?>

</div>

<!-- Mark Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('mark', 'Mark:'); ?>

    <?php echo Form::number('mark', null, ['class' => 'form-control']); ?>

</div>

<!-- Worksheet Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('worksheet_id', 'Worksheet Id:'); ?>

    <?php echo Form::text('worksheet_id', null, ['class' => 'form-control']); ?>

</div>

<!-- Challenge Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('challenge_id', 'Challenge Id:'); ?>

    <?php echo Form::text('challenge_id', null, ['class' => 'form-control']); ?>

</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <?php echo Form::submit('Save', ['class' => 'btn btn-primary']); ?>

    <a href="<?php echo route('worksheetAnswers.index'); ?>" class="btn btn-default">Cancel</a>
</div>
