#!/bin/zsh

function namespaceFromFile {
    grep -h 'namespace' $1 | sed 's/^namespace \(.*\);/\1/g' | sed 's/\\/\\\\/g'
}

setopt extended_glob

filelist=$(for file in app/Models/*/*.php; do echo `expr length $file` $file; done | sort -rn)

for file in app/Models/*/*.php
do
    modelFile=$(basename $file)
    modelName=$(echo $modelFile | cut -d. -f1)
    otherNamespace=$(namespaceFromFile $file)
    originalNamespace=App\\\\Models\\\\$modelName
    otherFullName=$otherNamespace\\\\$modelName
    originalFullName=$originalNamespace\\\\$modelName
    echo --- $otherFullName $originalFullName ---
    for filer in $(grep -l $(echo $originalFullName | sed 's/\\/\\\\/g') app/Models/*/*.php)
    do
        sed -i "s#${originalFullName}:#${otherFullName}:#g" $filer
    done
done
